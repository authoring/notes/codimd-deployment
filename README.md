CodiMD Deployment
=================

This is a simple Helm chart that deploys CodiMD (with some CERN-specific customizations) on OpenShift.
This could also be deployed directly on k8s, but keep in mind that `Route` resource is only specific to OpenShift,
thus it has to be replaced by an equivalent k8s resource.

In order to release a new version of codimd do the following:

```bash
oc login https://<openshift-url> --token=<token>
oc project <desired-project>

helm -n <desired-project> upgrade codimd .
```

To rollback a faulty release:

```bash
oc login https://<openshift-url> --token=<token>
oc project <desired-project>

helm -n <desired-project> rollback codimd
```
